#!/usr/bin/env python3
# coding=utf-8
import sys
import json
import requests

requests.packages.urllib3.disable_warnings()


class HTTP:
    server = None
    token = None

    @classmethod
    def get_token(cls, corpid, corpsecret):
        data = {"corpid": corpid, "corpsecret": corpsecret}
        url = "/cgi-bin/gettoken"
        res = requests.get(cls.server + url, data)
        res_data = res.json()
        if res.status_code in [200, 201] and res_data:
            token = res_data.get("access_token")
            cls.token = token
        else:
            print("获取 token 错误, 请检查输入项是否正确")
            sys.exit()

    @classmethod
    def post(cls, url, data=None, json=None, **kwargs):
        url = cls.server + url
        res = requests.post(url, data, json, **kwargs)
        return res


class Message(object):
    def __init__(self):
        self.toparty = toparty
        self.msgtype = "text"
        self.agentid = agentid
        self.subject = subject
        self.content = content

    def sendMsg(self):
        url = f"/cgi-bin/message/send?access_token={HTTP.token}"
        Data = {
            "toparty": self.toparty,
            "msgtype": "text",
            "agentid": self.agentid,
            "text": {"content": self.subject + "\n" + self.content},
            "safe": "0",
        }
        data = json.dumps(Data, ensure_ascii=False)
        res = HTTP.post(url=url, data=data, verify=False)
        print(res.json())


class SendMessage(object):
    def __init__(self):
        self.url = url
        self.corpid = Corpid
        self.secret = Secret
        self.token = None
        self.server = None

    def init_http(self):
        HTTP.server = self.url
        HTTP.get_token(self.corpid, self.secret)

    def perform(self):
        self.init_http()
        self.perm = Message()
        self.perm.sendMsg()


if __name__ == "__main__":
    url = "https://qyapi.weixin.qq.com"
    # 部门ID
    toparty = 1
    # 应用ID
    agentid = 1000001
    # 发送主题
    subject = ""
    # 发送内容
    content = "test"
    # CorpID是企业号的标识
    Corpid = ""
    # Secret是管理组凭证密钥
    Secret = ""
    api = SendMessage()
    api.perform()
