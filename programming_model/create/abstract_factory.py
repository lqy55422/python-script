from __future__ import annotations
from abc import ABC, abstractmethod


# AbstractFactory定义了创建AbstractProductA和AbstractProductB产品的接口
class AbstractFactory(ABC):
    @abstractmethod
    def create_product_a(self) -> AbstractProductA:
        """创建产品A的接口"""
        pass

    @abstractmethod
    def create_product_b(self) -> AbstractProductB:
        """创建产品B的接口"""
        pass


# ConcreteFactory1实现了AbstractFactory接口，生产的是ConcreteProductA1和ConcreteProductB1产品
class ConcreteFactory1(AbstractFactory):
    def create_product_a(self) -> AbstractProductA:
        """创建具体产品A1"""
        return ConcreteProductA1()

    def create_product_b(self) -> AbstractProductB:
        """创建具体产品B1"""
        return ConcreteProductB1()


# ConcreteFactory2也实现了AbstractFactory接口，但生产的是ConcreteProductA2和ConcreteProductB2产品
class ConcreteFactory2(AbstractFactory):
    def create_product_a(self) -> AbstractProductA:
        """创建具体产品A2"""
        return ConcreteProductA2()

    def create_product_b(self) -> AbstractProductB:
        """创建具体产品B2"""
        return ConcreteProductB2()


# AbstractProductA定义了ProductA类型产品的接口
class AbstractProductA(ABC):
    @abstractmethod
    def useful_function_a(self) -> str:
        """产品A的有用功能接口"""
        pass


# ConcreteProductA1和ConcreteProductA2是AbstractProductA的具体实现
class ConcreteProductA1(AbstractProductA):
    def useful_function_a(self) -> str:
        """具体产品A1的有用功能实现"""
        return "The result of the product A1."


class ConcreteProductA2(AbstractProductA):
    def useful_function_a(self) -> str:
        """具体产品A2的有用功能实现"""
        return "The result of the product A2."


# AbstractProductB定义了ProductB类型产品的接口，并声明了其可以与其他产品（如ProductA）协作
class AbstractProductB(ABC):
    @abstractmethod
    def useful_function_b(self) -> None:
        """产品B的有用功能接口"""
        pass

    @abstractmethod
    def another_useful_function_b(self, collaborator: AbstractProductA) -> None:
        """产品B与产品A协作的功能接口"""
        pass


# ConcreteProductB1和ConcreteProductB2是AbstractProductB的具体实现
class ConcreteProductB1(AbstractProductB):
    def useful_function_b(self) -> str:
        """具体产品B1的有用功能实现"""
        return "The result of the product B1."

    def another_useful_function_b(self, collaborator: AbstractProductA) -> str:
        """具体产品B1与产品A的协作功能实现"""
        result = collaborator.useful_function_a()
        return f"The result of the B1 collaborating with the ({result})"


class ConcreteProductB2(AbstractProductB):
    def useful_function_b(self) -> str:
        """具体产品B2的有用功能实现"""
        return "The result of the product B2."

    def another_useful_function_b(self, collaborator: AbstractProductA) -> str:
        """具体产品B2与产品A的协作功能实现"""
        result = collaborator.useful_function_a()
        return f"The result of the B2 collaborating with the ({result})"


# client_code函数演示了如何使用AbstractFactory接口和它创建的产品
def client_code(factory: AbstractFactory) -> None:
    """客户端代码，用于展示工厂生成的产品及其协作"""
    product_a = factory.create_product_a()
    product_b = factory.create_product_b()

    print(f"{product_b.useful_function_b()}")
    print(f"{product_b.another_useful_function_b(product_a)}", end="")


# 根据不同的工厂类型，client_code函数会得到不同类型的产品并展示它们的协作结果
if __name__ == "__main__":
    print("Client: Testing client code with the first factory type:")
    client_code(ConcreteFactory1())

    print("\n")

    print("Client: Testing the same client code with the second factory type:")
    client_code(ConcreteFactory2())
