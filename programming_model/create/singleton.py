class SingletonMeta(type):
    """
    Singleton类可以通过不同方式在Python中实现。可能的方法包括：基类，装饰器，和元类。我们将使用元类，因为它最适合此目的。
    """

    _instances = {}

    def __call__(cls, *args, **kwargs):
        """
        `__init__`参数值的任何改变不会影响返回的实例。
        """
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class Singleton(metaclass=SingletonMeta):
    """
    Singleton类，基于SingletonMeta元类实现确保实例唯一性。
    """

    def some_business_logic(self):
        """
        最终，任何单例都应该定义一些业务逻辑，这可以在其实例上执行。
        """

        # ...


if __name__ == "__main__":
    # 客户端代码。

    s1 = Singleton()
    s2 = Singleton()

    if id(s1) == id(s2):
        print("Singleton works, both variables contain the same instance.")
    else:
        print("Singleton failed, variables contain different instances.")
