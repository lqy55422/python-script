#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
from __future__ import annotations
from abc import ABC


class Mediator(ABC):
    """
    Mediator 接口声明了一个方法，用于组件通知中介者有关各种事件。
    中介者可以对这些事件作出反应，并将执行传递给其他组件。
    """

    def notify(self, sender: object, event: str) -> None:
        pass


class ConcreteMediator(Mediator):
    """
    ConcreteMediator 实现了 Mediator 接口，用于具体的中介职责。
    它协调两个组件之间的交互。
    """

    def __init__(self, component1: Component1, component2: Component2) -> None:
        self._component1 = component1
        self._component1.mediator = self
        self._component2 = component2
        self._component2.mediator = self

    def notify(self, sender: object, event: str) -> None:
        """
        根据接收到的事件，执行相应的操作。
        """
        if event == "A":
            print("Mediator reacts on A and triggers following operations:")
            self._component2.do_c()
        elif event == "D":
            print("Mediator reacts on D and triggers following operations:")
            self._component1.do_b()
            self._component2.do_c()


class BaseComponent:
    """
    BaseComponent 提供基本功能，将中介者的实例存储在组件对象中。
    """

    def __init__(self, mediator: Mediator = None) -> None:
        self._mediator = mediator

    @property
    def mediator(self) -> Mediator:
        return self._mediator

    @mediator.setter
    def mediator(self, mediator: Mediator) -> None:
        self._mediator = mediator


"""
Concrete Components 实现各种功能。它们不依赖于其他组件。
它们也不依赖于任何具体的中介者类。
"""


class Component1(BaseComponent):
    """
    Component1 具体组件，提供功能 A 和 B。
    它通过中介者进行通知。
    """

    def do_a(self) -> None:
        print("Component 1 does A.")
        self.mediator.notify(self, "A")

    def do_b(self) -> None:
        print("Component 1 does B.")
        self.mediator.notify(self, "B")


class Component2(BaseComponent):
    """
    Component2 具体组件，提供功能 C 和 D。
    它通过中介者进行通知。
    """

    def do_c(self) -> None:
        print("Component 2 does C.")
        self.mediator.notify(self, "C")

    def do_d(self) -> None:
        print("Component 2 does D.")
        self.mediator.notify(self, "D")


if __name__ == "__main__":
    # The client code.
    c1 = Component1()
    c2 = Component2()
    mediator = ConcreteMediator(c1, c2)

    print("Client triggers operation A.")
    c1.do_a()

    print("\n", end="")

    print("Client triggers operation D.")
    c2.do_d()
