#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
from abc import ABC, abstractmethod


class AbstractClass(ABC):
    """
    抽象类定义了一个模板方法，包含某个算法的骨架，通常由对抽象基本操作的调用组成。

    具体的子类应该实现这些操作，但保持模板方法本身不变。
    """

    def template_method(self) -> None:
        """
        模板方法定义了算法的骨架。
        """

        self.base_operation1()
        self.required_operations1()
        self.base_operation2()
        self.hook1()
        self.required_operations2()
        self.base_operation3()
        self.hook2()

    # 这些操作已经有实现。

    def base_operation1(self) -> None:
        print("AbstractClass says: I am doing the bulk of the work")

    def base_operation2(self) -> None:
        print("AbstractClass says: But I let subclasses override some operations")

    def base_operation3(self) -> None:
        print("AbstractClass says: But I am doing the bulk of the work anyway")

    # 这些操作必须在子类中实现。

    @abstractmethod
    def required_operations1(self) -> None:
        pass

    @abstractmethod
    def required_operations2(self) -> None:
        pass

    # 这些是“钩子”。子类可以重写它们，但这不是强制性的，因为钩子已经有默认（但空的）实现。钩子
    # 在算法的一些关键位置提供额外的扩展点。

    def hook1(self) -> None:
        pass

    def hook2(self) -> None:
        pass


class ConcreteClass1(AbstractClass):
    """
    具体类必须实现基类的所有抽象操作。它们也可以用默认实现重写某些操作。
    """

    def required_operations1(self) -> None:
        print("ConcreteClass1 says: Implemented Operation1")

    def required_operations2(self) -> None:
        print("ConcreteClass1 says: Implemented Operation2")


class ConcreteClass2(AbstractClass):
    """
    通常，具体类只会重写基类的部分操作。
    """

    def required_operations1(self) -> None:
        print("ConcreteClass2 says: Implemented Operation1")

    def required_operations2(self) -> None:
        print("ConcreteClass2 says: Implemented Operation2")

    def hook1(self) -> None:
        print("ConcreteClass2 says: Overridden Hook1")


def client_code(abstract_class: AbstractClass) -> None:
    """
    客户端代码调用模板方法来执行算法。客户端代码不必知道与之工作的对象的具体类，
    只要它通过基类的接口与对象进行工作。
    """

    # ...
    abstract_class.template_method()
    # ...


if __name__ == "__main__":
    print("同一个客户端代码可以与不同的子类一起工作：")
    client_code(ConcreteClass1())
    print("")

    print("同一个客户端代码可以与不同的子类一起工作：")
    client_code(ConcreteClass2())
