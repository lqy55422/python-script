#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
from __future__ import annotations
from abc import ABC, abstractmethod


class Context:
    """
    上下文定义了客户感兴趣的接口。它还保持对状态子类实例的引用，
    该实例表示上下文的当前状态。
    """

    _state = None
    """
    当前上下文状态的引用。
    """

    def __init__(self, state: State) -> None:
        """
        初始化上下文，并切换到给定的状态。
        """
        self.transition_to(state)

    def transition_to(self, state: State):
        """
        上下文允许在运行时更改状态对象。
        """
        print(f"Context: Transition to {type(state).__name__}")
        self._state = state
        self._state.context = self

    """
    上下文将其部分行为委托给当前的状态对象。
    """

    def request1(self):
        """
        调用当前状态对象的handle1方法。
        """
        self._state.handle1()

    def request2(self):
        """
        调用当前状态对象的handle2方法。
        """
        self._state.handle2()


class State(ABC):
    """
    基状态类声明了所有具体状态应实现的方法，
    并提供与状态关联的上下文对象的后引用。
    该后引用可用于状态将上下文转换为另一状态。
    """

    @property
    def context(self) -> Context:
        """
        获取与状态关联的上下文对象。
        """
        return self._context

    @context.setter
    def context(self, context: Context) -> None:
        """
        设置与状态关联的上下文对象。
        """
        self._context = context

    @abstractmethod
    def handle1(self) -> None:
        """
        处理请求1的抽象方法。
        """
        pass

    @abstractmethod
    def handle2(self) -> None:
        """
        处理请求2的抽象方法。
        """
        pass


"""
具体状态实现与上下文状态相关的各种行为。
"""


class ConcreteStateA(State):
    def handle1(self) -> None:
        """
        处理请求1的具体实现，并尝试改变上下文的状态。
        """
        print("ConcreteStateA handles request1.")
        print("ConcreteStateA wants to change the state of the context.")
        self.context.transition_to(ConcreteStateB())

    def handle2(self) -> None:
        """
        处理请求2的具体实现。
        """
        print("ConcreteStateA handles request2.")


class ConcreteStateB(State):
    def handle1(self) -> None:
        """
        处理请求1的具体实现。
        """
        print("ConcreteStateB handles request1.")

    def handle2(self) -> None:
        """
        处理请求2的具体实现，并尝试改变上下文的状态。
        """
        print("ConcreteStateB handles request2.")
        print("ConcreteStateB wants to change the state of the context.")
        self.context.transition_to(ConcreteStateA())


if __name__ == "__main__":
    # 客户端代码。

    context = Context(ConcreteStateA())
    context.request1()
    context.request2()
