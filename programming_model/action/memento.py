#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
from __future__ import annotations
from abc import ABC, abstractmethod
from datetime import datetime
from random import sample
from string import ascii_letters


class Originator:
    """
    Originator类持有一些重要的状态，这些状态可能会随着时间的推移而改变。
    它还定义了一种将状态保存到备忘录中的方法，以及从中恢复状态的另一种方法。
    """

    _state = None
    """
    为了简化起见，Originator的状态存储在一个变量中。
    """

    def __init__(self, state: str) -> None:
        self._state = state
        print(f"Originator: My initial state is: {self._state}")

    def do_something(self) -> None:
        """
        Originator的业务逻辑可能会影响其内部状态。
        因此，客户端应该在通过save()方法启动业务逻辑的方法之前备份状态。
        """

        print("Originator: I'm doing something important.")
        self._state = self._generate_random_string(30)
        print(f"Originator: and my state has changed to: {self._state}")

    @staticmethod
    def _generate_random_string(length: int = 10) -> str:
        """
        生成指定长度的随机字符串。
        """
        return "".join(sample(ascii_letters, length))

    def save(self) -> Memento:
        """
        将当前状态保存在备忘录中。
        """

        return ConcreteMemento(self._state)

    def restore(self, memento: Memento) -> None:
        """
        从备忘录对象恢复Originator的状态。
        """

        self._state = memento.get_state()
        print(f"Originator: My state has changed to: {self._state}")


class Memento(ABC):
    """
    Memento接口提供了一种获取备忘录元数据的方法，如创建日期或名称。
    但是，它不暴露Originator的状态。
    """

    @abstractmethod
    def get_name(self) -> str:
        pass

    @abstractmethod
    def get_date(self) -> str:
        pass


class ConcreteMemento(Memento):
    """
    ConcreteMemento类实现了Memento接口，用于保存Originator的状态。
    """

    def __init__(self, state: str) -> None:
        self._state = state
        self._date = str(datetime.now())[:19]

    def get_state(self) -> str:
        """
        Originator在恢复其状态时使用此方法。
        """
        return self._state

    def get_name(self) -> str:
        """
        其余方法由Caretaker用于显示元数据。
        """

        return f"{self._date} / ({self._state[0:9]}...)"

    def get_date(self) -> str:
        return self._date


class Caretaker:
    """
    Caretaker与Concrete Memento类无关。
    因此，它无法访问存储在备忘录中的Originator的状态。
    它通过基本的Memento接口处理所有备忘录。
    """

    def __init__(self, originator: Originator) -> None:
        self._mementos = []
        self._originator = originator

    def backup(self) -> None:
        """
        备份Originator的状态。
        """
        print("\nCaretaker: Saving Originator's state...")
        self._mementos.append(self._originator.save())

    def undo(self) -> None:
        """
        恢复到之前的状态。
        """
        if not len(self._mementos):
            return

        memento = self._mementos.pop()
        print(f"Caretaker: Restoring state to: {memento.get_name()}")
        try:
            self._originator.restore(memento)
        except Exception:
            self.undo()

    def show_history(self) -> None:
        """
        显示备忘录的历史记录。
        """
        print("Caretaker: Here's the list of mementos:")
        for memento in self._mementos:
            print(memento.get_name())


if __name__ == "__main__":
    originator = Originator("Super-duper-super-puper-super.")
    caretaker = Caretaker(originator)

    caretaker.backup()
    originator.do_something()

    caretaker.backup()
    originator.do_something()

    caretaker.backup()
    originator.do_something()

    print()
    caretaker.show_history()

    print("\nClient: Now, let's rollback!\n")
    caretaker.undo()

    print("\nClient: Once more!\n")
    caretaker.undo()
