#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
from __future__ import annotations
from abc import ABC, abstractmethod
from random import randrange
from typing import List


class Subject(ABC):
    """
    Subject接口声明了一组管理订阅者的方法。
    """

    @abstractmethod
    def attach(self, observer: Observer) -> None:
        """
        将观察者附加到主题。
        """
        pass

    @abstractmethod
    def detach(self, observer: Observer) -> None:
        """
        从主题中分离观察者。
        """
        pass

    @abstractmethod
    def notify(self) -> None:
        """
        通知所有观察者有关事件的发生。
        """
        pass


class ConcreteSubject(Subject):
    """
    主题拥有一些重要的状态，并在状态变化时通知观察者。
    """

    _state: int = None
    """
    为了简单起见，主题的状态（对所有订阅者至关重要）存储在此变量中。
    """

    _observers: List[Observer] = []
    """
    订阅者列表。在现实中，订阅者列表可以更全面地存储（按事件类型分类等）。
    """

    def attach(self, observer: Observer) -> None:
        print("Subject: Attached an observer.")
        self._observers.append(observer)

    def detach(self, observer: Observer) -> None:
        self._observers.remove(observer)

    """
    订阅管理方法。
    """

    def notify(self) -> None:
        """
        在每个订阅者中触发更新。
        """

        print("Subject: Notifying observers...")
        for observer in self._observers:
            observer.update(self)

    def some_business_logic(self) -> None:
        """
        通常，订阅逻辑只是主题可以真正执行的部分。主题通常持有一些重要的业务逻辑，
        每当某些重要事情即将发生（或之后）时，会触发通知方法。
        """

        print("\nSubject: I'm doing something important.")
        self._state = randrange(0, 10)

        print(f"Subject: My state has just changed to: {self._state}")
        self.notify()


class Observer(ABC):
    """
    Observer接口声明了由主题使用的更新方法。
    """

    @abstractmethod
    def update(self, subject: Subject) -> None:
        """
        接收主题的更新。
        """
        pass


"""
具体观察者对它们所附加的主题发出的更新作出反应。
"""


class ConcreteObserverA(Observer):
    def update(self, subject: Subject) -> None:
        if subject._state < 3:
            print("ConcreteObserverA: Reacted to the event")


class ConcreteObserverB(Observer):
    def update(self, subject: Subject) -> None:
        if subject._state == 0 or subject._state >= 2:
            print("ConcreteObserverB: Reacted to the event")


if __name__ == "__main__":
    # 客户端代码。

    subject = ConcreteSubject()

    observer_a = ConcreteObserverA()
    subject.attach(observer_a)

    observer_b = ConcreteObserverB()
    subject.attach(observer_b)

    subject.some_business_logic()
    subject.some_business_logic()

    subject.detach(observer_a)

    subject.some_business_logic()
