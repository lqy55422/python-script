#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
from __future__ import annotations
from abc import ABC, abstractmethod
from typing import List


class Component(ABC):
    """
    组件接口声明一个 `accept` 方法，该方法应该接收基础访问者接口作为参数。
    """

    @abstractmethod
    def accept(self, visitor: Visitor) -> None:
        pass


class ConcreteComponentA(Component):
    """
    每个具体组件必须以一种方式实现 `accept` 方法，
    使其调用与组件类对应的访问者方法。
    """

    def accept(self, visitor: Visitor) -> None:
        """
        请注意，我们调用 `visitConcreteComponentA`，它与当前类名匹配。
        这样我们让访问者知道它所处理的组件的类。
        """

        visitor.visit_concrete_component_a(self)

    def exclusive_method_of_concrete_component_a(self) -> str:
        """
        具体组件可能有一些在其基类或接口中不存在的特殊方法。
        访问者仍然能够使用这些方法，因为它知道组件的具体类。
        """

        return "A"


class ConcreteComponentB(Component):
    """
    同样的逻辑：visitConcreteComponentB => ConcreteComponentB
    """

    def accept(self, visitor: Visitor):
        visitor.visit_concrete_component_b(self)

    def special_method_of_concrete_component_b(self) -> str:
        return "B"


class Visitor(ABC):
    """
    访问者接口声明了一组与组件类对应的访问方法。
    访问方法的签名允许访问者识别它正在处理的确切组件类。
    """

    @abstractmethod
    def visit_concrete_component_a(self, element: ConcreteComponentA) -> None:
        pass

    @abstractmethod
    def visit_concrete_component_b(self, element: ConcreteComponentB) -> None:
        pass


"""
具体访问者实现了相同算法的多个版本，这些算法可以与所有具体组件类一起使用。

当在复杂对象结构（例如复合树）中使用访问者模式时，
您可以体验到最大好处。在这种情况下，可能有必要在对结构中
各种对象执行访问者方法时存储一些算法的中间状态。
"""


class ConcreteVisitor1(Visitor):
    def visit_concrete_component_a(self, element) -> None:
        print(
            f"{element.exclusive_method_of_concrete_component_a()} + ConcreteVisitor1"
        )

    def visit_concrete_component_b(self, element) -> None:
        print(f"{element.special_method_of_concrete_component_b()} + ConcreteVisitor1")


class ConcreteVisitor2(Visitor):
    def visit_concrete_component_a(self, element) -> None:
        print(
            f"{element.exclusive_method_of_concrete_component_a()} + ConcreteVisitor2"
        )

    def visit_concrete_component_b(self, element) -> None:
        print(f"{element.special_method_of_concrete_component_b()} + ConcreteVisitor2")


def client_code(components: List[Component], visitor: Visitor) -> None:
    """
    客户端代码可以在任何元素集合上运行访问者操作，而无需
    理解它们的具体类。accept 操作将调用导向
    访问者对象中的适当操作。
    """

    # ...
    for component in components:
        component.accept(visitor)
    # ...


if __name__ == "__main__":
    components = [ConcreteComponentA(), ConcreteComponentB()]

    print("客户端代码通过基础访问者接口与所有访问者工作：")
    visitor1 = ConcreteVisitor1()
    client_code(components, visitor1)

    print("这允许相同的客户端代码与不同类型的访问者一起工作：")
    visitor2 = ConcreteVisitor2()
    client_code(components, visitor2)
