#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
from __future__ import annotations
from abc import ABC, abstractmethod


class Command(ABC):
    """
    命令接口声明了一个执行命令的方法。
    """

    @abstractmethod
    def execute(self) -> None:
        pass


class SimpleCommand(Command):
    """
    一些命令可以独立实现简单的操作。
    """

    def __init__(self, payload: str) -> None:
        self._payload = payload

    def execute(self) -> None:
        print(
            f"SimpleCommand: See, I can do simple things like printing"
            f"({self._payload})"
        )


class ComplexCommand(Command):
    """
    然而，某些命令可以将更复杂的操作委托给其他
    对象，称为“接收者”。
    """

    def __init__(self, receiver: Receiver, a: str, b: str) -> None:
        """
        复杂命令可以通过构造函数接受一个或多个接收者对象以及
        任何上下文数据。
        """

        self._receiver = receiver
        self._a = a
        self._b = b

    def execute(self) -> None:
        """
        命令可以委托给接收者的任何方法。
        """

        print(
            "ComplexCommand: Complex stuff should be done by a receiver object", end=""
        )
        self._receiver.do_something(self._a)
        self._receiver.do_something_else(self._b)


class Receiver:
    """
    接收者类包含一些重要的业务逻辑。它们知道如何
    执行与处理请求相关的各种操作。实际上，任何类都可以作为接收者。
    """

    def do_something(self, a: str) -> None:
        print(f"\nReceiver: Working on ({a}.)", end="")

    def do_something_else(self, b: str) -> None:
        print(f"\nReceiver: Also working on ({b}.)", end="")


class Invoker:
    """
    调用者与一个或多个命令相关联。它向命令发送请求。
    """

    _on_start = None
    _on_finish = None

    """
    初始化命令。
    """

    def set_on_start(self, command: Command):
        self._on_start = command

    def set_on_finish(self, command: Command):
        self._on_finish = command

    def do_something_important(self) -> None:
        """
        调用者不依赖于具体的命令或接收者类。调用者通过执行
        命令间接地将请求传递给接收者。
        """

        print("Invoker: Does anybody want something done before I begin?")
        if isinstance(self._on_start, Command):
            self._on_start.execute()

        print("Invoker: ...doing something really important...")

        print("Invoker: Does anybody want something done after I finish?")
        if isinstance(self._on_finish, Command):
            self._on_finish.execute()


if __name__ == "__main__":
    """
    客户端代码可以用任何命令参数化一个调用者。
    """

    invoker = Invoker()
    invoker.set_on_start(SimpleCommand("Say Hi!"))
    receiver = Receiver()
    invoker.set_on_finish(ComplexCommand(receiver, "Send email", "Save report"))

    invoker.do_something_important()
