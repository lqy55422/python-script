#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
from __future__ import annotations
from collections.abc import Iterable, Iterator
from typing import Any


"""
要在Python中创建迭代器，有两个抽象类来自内置的`collections`模块 - Iterable和Iterator。
我们需要在被迭代的对象（集合）中实现`__iter__()`方法，在迭代器中实现`__next__()`方法。
"""


class AlphabeticalOrderIterator(Iterator):
    """
    具体的迭代器实现各种遍历算法。这些类始终存储当前的遍历位置。
    """

    """
    `_position`属性存储当前的遍历位置。迭代器可能有许多其他字段用于存储迭代状态，
    特别是在它应该与特定类型的集合一起工作时。
    """
    _position: int = None

    """
    此属性指示遍历方向。
    """
    _reverse: bool = False

    def __init__(self, collection: WordsCollection, reverse: bool = False) -> None:
        """
        初始化字母顺序迭代器，设置集合和遍历方向。
        """
        self._collection = collection
        self._reverse = reverse
        self._position = -1 if reverse else 0

    def __next__(self) -> Any:
        """
        __next__()方法必须返回序列中的下一个项目。在到达结尾时，以及在后续调用中，
        它必须引发StopIteration异常。
        """
        try:
            value = self._collection[self._position]
            self._position += -1 if self._reverse else 1
        except IndexError:
            raise StopIteration()

        return value


class WordsCollection(Iterable):
    """
    具体的集合提供一种或几种方法来获取与集合类兼容的新迭代器实例。
    """

    def __init__(self, collection: list[Any] | None = None) -> None:
        """
        初始化单词集合，设置集合项的存储。
        """
        self._collection = collection or []

    def __getitem__(self, index: int) -> Any:
        """
        获取指定索引的集合项。
        """
        return self._collection[index]

    def __iter__(self) -> AlphabeticalOrderIterator:
        """
        __iter__()方法返回迭代器对象本身，默认返回升序的迭代器。
        """
        return AlphabeticalOrderIterator(self)

    def get_reverse_iterator(self) -> AlphabeticalOrderIterator:
        """
        获取反向迭代器，以便以降序方式迭代集合。
        """
        return AlphabeticalOrderIterator(self, True)

    def add_item(self, item: Any) -> None:
        """
        向集合中添加一个新项。
        """
        self._collection.append(item)


if __name__ == "__main__":
    # 客户端代码可能知道或不知道具体的迭代器或集合类，这取决于您希望在程序中保持的间接级别。
    collection = WordsCollection()
    collection.add_item("First")
    collection.add_item("Second")
    collection.add_item("Third")

    print("Straight traversal:")
    print("\n".join(collection))
    print("")

    print("Reverse traversal:")
    print("\n".join(collection.get_reverse_iterator()), end="")
