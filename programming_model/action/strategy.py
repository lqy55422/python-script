#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
from __future__ import annotations
from abc import ABC, abstractmethod
from typing import List


class Context:
    """
    上下文定义了客户端感兴趣的接口。
    """

    def __init__(self, strategy: Strategy) -> None:
        """
        通常，上下文通过构造函数接受策略，但也提供了一个在运行时更改它的设置方法。
        """

        self._strategy = strategy

    @property
    def strategy(self) -> Strategy:
        """
        上下文维护对一个策略对象的引用。上下文不知道策略的具体类，它应该通过策略接口与所有策略一起工作。
        """

        return self._strategy

    @strategy.setter
    def strategy(self, strategy: Strategy) -> None:
        """
        通常，上下文允许在运行时替换策略对象。
        """

        self._strategy = strategy

    def do_some_business_logic(self) -> None:
        """
        上下文将一些工作委托给策略对象，而不是自己实现多个版本的算法。
        """

        # ...

        print("Context: Sorting data using the strategy (not sure how it'll do it)")
        result = self._strategy.do_algorithm(["a", "b", "c", "d", "e"])
        print(",".join(result))

        # ...


class Strategy(ABC):
    """
    策略接口声明了对支持的某些算法的所有版本共同的操作。

    上下文使用这个接口调用由具体策略定义的算法。
    """

    @abstractmethod
    def do_algorithm(self, data: List):
        pass


"""
具体策略实现算法，同时遵循基本策略接口。
该接口使它们在上下文中可互换。
"""


class ConcreteStrategyA(Strategy):
    """
    具体策略A实现了一个正常排序的算法。
    """

    def do_algorithm(self, data: List) -> List:
        return sorted(data)


class ConcreteStrategyB(Strategy):
    """
    具体策略B实现了一个反向排序的算法。
    """

    def do_algorithm(self, data: List) -> List:
        return reversed(sorted(data))


if __name__ == "__main__":
    # 客户端代码选择一个具体策略并将其传递给上下文。
    # 客户端应该了解策略之间的差异，以便做出正确的选择。

    context = Context(ConcreteStrategyA())
    print("Client: Strategy is set to normal sorting.")
    context.do_some_business_logic()
    print()

    print("Client: Strategy is set to reverse sorting.")
    context.strategy = ConcreteStrategyB()
    context.do_some_business_logic()
