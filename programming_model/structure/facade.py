from __future__ import annotations


class Facade:
    """
    Facade类为一个或多个子系统的复杂逻辑提供一个简单的接口。
    Facade将客户端请求委托给子系统内的适当对象。Facade还负责管理它们的生命周期。
    所有这些都将客户端与子系统的不必要复杂性隔离开来。
    """

    def __init__(self, subsystem1: Subsystem1, subsystem2: Subsystem2) -> None:
        """
        根据应用程序的需要，可以向Facade提供现有的子系统对象，
        或者强制Facade自己创建这些对象。
        """

        self._subsystem1 = subsystem1 or Subsystem1()
        self._subsystem2 = subsystem2 or Subsystem2()

    def operation(self) -> str:
        """
        Facade的方法是子系统复杂功能的方便快捷方式。
        然而，客户只能够访问子系统功能的一小部分。
        """

        results = []
        results.append("Facade initializes subsystems:")
        results.append(self._subsystem1.operation1())
        results.append(self._subsystem2.operation1())
        results.append("Facade orders subsystems to perform the action:")
        results.append(self._subsystem1.operation_n())
        results.append(self._subsystem2.operation_z())
        return "\n".join(results)


class Subsystem1:
    """
    子系统可以直接接受来自Facade或客户端的请求。
    无论如何，对于子系统来说，Facade仍然是另一个客户端，
    而且它并不是子系统的一部分。
    """

    def operation1(self) -> str:
        return "Subsystem1: Ready!"

    # ...

    def operation_n(self) -> str:
        return "Subsystem1: Go!"


class Subsystem2:
    """
    一些Facade可以同时处理多个子系统。
    """

    def operation1(self) -> str:
        return "Subsystem2: Get ready!"

    # ...

    def operation_z(self) -> str:
        return "Subsystem2: Fire!"


def client_code(facade: Facade) -> None:
    """
    客户端代码通过Facade提供的简单接口与复杂子系统交互。
    当Facade管理子系统的生命周期时，客户端甚至可能不知道
    子系统的存在。这种方法使您能够保持复杂性在可控范围内。
    """

    print(facade.operation(), end="")


if __name__ == "__main__":
    # 客户端代码可能已经创建了一些子系统的对象。
    # 在这种情况下，将子系统对象初始化到Facade中可能是值得的，
    # 而不是让Facade创建新的实例。
    subsystem1 = Subsystem1()
    subsystem2 = Subsystem2()
    facade = Facade(subsystem1, subsystem2)
    client_code(facade)
