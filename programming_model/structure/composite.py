from __future__ import annotations
from abc import ABC, abstractmethod
from typing import List


class Component(ABC):
    """
    基础组件类声明了组成部分的简单和复杂对象的公共操作。
    """

    @property
    def parent(self) -> Component:
        return self._parent

    @parent.setter
    def parent(self, parent: Component):
        """
        可选地，基础组件可以声明一个接口，用于设置和访问组件在树结构中的父级。
        它还可以为这些方法提供一些默认实现。
        """

        self._parent = parent

    """
    在某些情况下，将子管理操作直接定义在基础组件类中是有益的。
    这样，即使在对象树组装期间，您也不需要向客户端代码公开任何具体组件类。
    缺点是这些方法对于叶级组件来说将是空的。
    """

    def add(self, component: Component) -> None:
        pass

    def remove(self, component: Component) -> None:
        pass

    def is_composite(self) -> bool:
        """
        提供一个方法，让客户端代码判断一个组件是否可以拥有子级。
        """

        return False

    @abstractmethod
    def operation(self) -> str:
        """
        基础组件可以实现一些默认行为或将其留给具体类（通过将包含行为的方法声明为“抽象”）。
        """

        pass


class Leaf(Component):
    """
    叶类表示组成的最终对象。叶子不能有任何子级。

    通常，实际工作由叶子对象完成，而复合对象只委托给其子组件。
    """

    def operation(self) -> str:
        return "Leaf"


class Composite(Component):
    """
    复合类表示可能具有子级的复杂组件。通常，复合对象将实际工作委托给其子级，
    然后“汇总”结果。
    """

    def __init__(self) -> None:
        self._children: List[Component] = []

    """
    复合对象可以向其子级列表添加或移除其他组件（简单或复杂）。
    """

    def add(self, component: Component) -> None:
        self._children.append(component)
        component.parent = self

    def remove(self, component: Component) -> None:
        self._children.remove(component)
        component.parent = None

    def is_composite(self) -> bool:
        return True

    def operation(self) -> str:
        """
        复合执行其主要逻辑的特定方式。它通过递归遍历所有子级，
        收集并汇总它们的结果。因为复合的子级将这些调用传递给它们的子级，
        所以最终遍历了整个对象树。
        """

        results = []
        for child in self._children:
            results.append(child.operation())
        return f"Branch({'+'.join(results)})"


def client_code(component: Component) -> None:
    """
    客户端代码通过基础接口与所有组件一起工作。
    """

    print(f"RESULT: {component.operation()}", end="")


def client_code2(component1: Component, component2: Component) -> None:
    """
    由于子管理操作在基础组件类中声明，客户端代码可以与任何组件，
    无论是简单的还是复杂的，而无需依赖它们的具体类。
    """

    if component1.is_composite():
        component1.add(component2)

    print(f"RESULT: {component1.operation()}", end="")


if __name__ == "__main__":
    # 通过这种方式，客户端代码可以支持简单的叶子组件...
    simple = Leaf()
    print("Client: I've got a simple component:")
    client_code(simple)
    print("\n")

    # ...以及复杂的复合体。
    tree = Composite()

    branch1 = Composite()
    branch1.add(Leaf())
    branch1.add(Leaf())

    branch2 = Composite()
    branch2.add(Leaf())

    tree.add(branch1)
    tree.add(branch2)

    print("Client: Now I've got a composite tree:")
    client_code(tree)
    print("\n")

    print(
        "Client: I don't need to check the components classes even when managing the tree:"
    )
    client_code2(tree, simple)
