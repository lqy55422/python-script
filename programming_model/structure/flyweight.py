#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
import json
from typing import Dict


class Flyweight:
    """
    Flyweight 类用于存储多个真实业务实体的共有状态部分（也称为内在状态）。
    Flyweight 接受其方法参数中包含的其他状态（外在状态，每个实体唯一）。
    """

    def __init__(self, shared_state: str) -> None:
        self._shared_state = shared_state

    def operation(self, unique_state: str) -> None:
        """
        执行操作并显示共有状态和独特状态。
        """
        s = json.dumps(self._shared_state)
        u = json.dumps(unique_state)
        print(f"Flyweight: Displaying shared ({s}) and unique ({u}) state.", end="")


class FlyweightFactory:
    """
    FlyweightFactory 类创建和管理 Flyweight 对象。它确保正确共享 flyweights。
    当客户端请求一个 flyweight 时，工厂要么返回一个现有实例，要么创建一个新实例（如果尚不存在）。
    """

    _flyweights: Dict[str, Flyweight] = {}

    def __init__(self, initial_flyweights: Dict) -> None:
        """
        初始化 FlyweightFactory 类并创建初始的 Flyweight 对象。
        """
        for state in initial_flyweights:
            self._flyweights[self.get_key(state)] = Flyweight(state)

    def get_key(self, state: Dict) -> str:
        """
        返回给定状态的 Flyweight 的字符串哈希。
        """

        return "_".join(sorted(state))

    def get_flyweight(self, shared_state: Dict) -> Flyweight:
        """
        根据给定状态返回现有 Flyweight，或者创建一个新 Flyweight。
        """

        key = self.get_key(shared_state)

        if not self._flyweights.get(key):
            print("FlyweightFactory: Can't find a flyweight, creating new one.")
            self._flyweights[key] = Flyweight(shared_state)
        else:
            print("FlyweightFactory: Reusing existing flyweight.")

        return self._flyweights[key]

    def list_flyweights(self) -> None:
        """
        列出当前 FlyweightFactory 中的所有 Flyweight 对象。
        """
        count = len(self._flyweights)
        print(f"FlyweightFactory: I have {count} flyweights:")
        print("\n".join(map(str, self._flyweights.keys())), end="")


def add_car_to_police_database(
    factory: FlyweightFactory,
    plates: str,
    owner: str,
    brand: str,
    model: str,
    color: str,
) -> None:
    """
    向警察数据库添加汽车信息。
    """
    print("\n\nClient: Adding a car to database.")
    flyweight = factory.get_flyweight([brand, model, color])
    # The client code either stores or calculates extrinsic state and passes it
    # to the flyweight's methods.
    flyweight.operation([plates, owner])


if __name__ == "__main__":
    """
    客户端代码通常在应用程序初始化阶段创建预填充的 flyweights。
    """

    factory = FlyweightFactory(
        [
            ["Chevrolet", "Camaro2018", "pink"],
            ["Mercedes Benz", "C300", "black"],
            ["Mercedes Benz", "C500", "red"],
            ["BMW", "M5", "red"],
            ["BMW", "X6", "white"],
        ]
    )

    factory.list_flyweights()

    add_car_to_police_database(factory, "CL234IR", "James Doe", "BMW", "M5", "red")

    add_car_to_police_database(factory, "CL234IR", "James Doe", "BMW", "X1", "red")

    print("\n")

    factory.list_flyweights()
