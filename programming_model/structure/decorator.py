class Component:
    """
    基础组件接口定义可以被装饰器修改的操作。
    """

    def operation(self) -> str:
        pass


class ConcreteComponent(Component):
    """
    具体组件提供操作的默认实现。可能有多个变体。
    """

    def operation(self) -> str:
        return "ConcreteComponent"


class Decorator(Component):
    """
    基础装饰器类遵循与其他组件相同的接口。
    该类的主要目的是为所有具体装饰器定义包装接口。
    包装代码的默认实现可能包括用于存储被包装组件的字段以及初始化它的手段。
    """

    _component: Component = None

    def __init__(self, component: Component) -> None:
        self._component = component

    @property
    def component(self) -> Component:
        """
        装饰器将所有工作委托给被包装的组件。
        """

        return self._component

    def operation(self) -> str:
        return self._component.operation()


class ConcreteDecoratorA(Decorator):
    """
    具体装饰器调用被包装对象并以某种方式修改其结果。
    """

    def operation(self) -> str:
        """
        装饰器可以调用操作的父实现，而不是直接调用被包装对象。
        这种方法简化了装饰器类的扩展。
        """
        return f"ConcreteDecoratorA({self.component.operation()})"


class ConcreteDecoratorB(Decorator):
    """
    装饰器可以在调用被包装对象之前或之后执行其行为。
    """

    def operation(self) -> str:
        return f"ConcreteDecoratorB({self.component.operation()})"


def client_code(component: Component) -> None:
    """
    客户端代码与所有对象使用组件接口进行交互。
    这样，它可以保持独立于与之交互的具体组件类。
    """

    # ...

    print(f"RESULT: {component.operation()}", end="")

    # ...


if __name__ == "__main__":
    # 这样客户端代码可以支持简单组件...
    simple = ConcreteComponent()
    print("Client: I've got a simple component:")
    client_code(simple)
    print("\n")

    # ...以及装饰过的组件。
    #
    # 注意装饰器不仅可以包装简单组件，还可以包装其他装饰器。
    decorator1 = ConcreteDecoratorA(simple)
    decorator2 = ConcreteDecoratorB(decorator1)
    print("Client: Now I've got a decorated component:")
    client_code(decorator2)
