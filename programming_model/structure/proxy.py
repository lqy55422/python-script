#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
from abc import ABC, abstractmethod


class Subject(ABC):
    """
    Subject 接口声明了 RealSubject 和 Proxy 的共同操作。
    客户端只需通过这个接口与 RealSubject 工作，就可以将 Proxy 传递到请求中。
    """

    @abstractmethod
    def request(self) -> None:
        pass


class RealSubject(Subject):
    """
    RealSubject 包含一些核心业务逻辑。
    通常，RealSubject 能够执行一些有用的工作，这些工作可能很慢或敏感 -
    例如，纠正输入数据。Proxy 可以在不改变 RealSubject 代码的情况下解决这些问题。
    """

    def request(self) -> None:
        print("RealSubject: Handling request.")


class Proxy(Subject):
    """
    Proxy 拥有与 RealSubject 相同的接口。
    """

    def __init__(self, real_subject: RealSubject) -> None:
        self._real_subject = real_subject

    def request(self) -> None:
        """
        Proxy 模式最常见的应用包括延迟加载、缓存、访问控制、日志记录等。
        Proxy 可以执行其中一个操作，然后根据结果，将执行传递到关联的 RealSubject 对象的同一方法。
        """

        if self.check_access():
            self._real_subject.request()
            self.log_access()

    def check_access(self) -> bool:
        print("Proxy: Checking access prior to firing a real request.")
        return True

    def log_access(self) -> None:
        print("Proxy: Logging the time of request.", end="")


def client_code(subject: Subject) -> None:
    """
    客户端代码应该通过 Subject 接口与所有对象（包括主题和代理）进行交互，
    以支持真实主题和代理。在现实中，客户端通常直接与真实主题进行工作。
    在这种情况下，为了更轻松地实现该模式，可以从真实主题的类扩展你的代理。
    """

    # ...

    subject.request()

    # ...


if __name__ == "__main__":
    print("Client: Executing the client code with a real subject:")
    real_subject = RealSubject()
    client_code(real_subject)

    print("")

    print("Client: Executing the same client code with a proxy:")
    proxy = Proxy(real_subject)
    client_code(proxy)
