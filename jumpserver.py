#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import sys
import requests


class HTTP:
    server = None
    token = None

    @classmethod
    def get_token(cls, username, password):
        data = {"username": username, "password": password}
        url = "/api/v1/authentication/auth/"
        res = requests.post(cls.server + url, data)
        res_data = res.json()
        if res.status_code in [200, 201] and res_data:
            token = res_data.get("token")
            cls.token = token
        else:
            print("获取 token 错误, 请检查输入项是否正确")
            sys.exit()

    @classmethod
    def get(cls, url, params=None, **kwargs):
        url = cls.server + url
        headers = {"Authorization": "Bearer {}".format(cls.token), "X-JMS-ORG": "00000000-0000-0000-0000-000000000002"}
        kwargs["headers"] = headers
        res = requests.get(url, params, **kwargs)
        return res

    @classmethod
    def post(cls, url, data=None, json=None, **kwargs):
        url = cls.server + url
        headers = {"Authorization": "Bearer {}".format(cls.token), "X-JMS-ORG": "00000000-0000-0000-0000-000000000002"}
        kwargs["headers"] = headers
        res = requests.post(url, data, json, **kwargs)
        return res


class Node(object):
    def __init__(self):
        self.id = None
        self.name = asset_node_name

    def get(self):
        url = "/api/v1/assets/nodes/"
        res = HTTP.get(url)
        res_data = res.json()
        if res.status_code in [200, 201] and res_data:
            for res in res_data:
                if res["full_value"] == self.name:
                    self.id = res.get("id")
        return self.id


class AdminUser(object):
    # jumpserver 系统中管理用户
    def __init__(self):
        self.id = None
        self.name = assets_admin_name

    def get(self):
        url = "/api/v1/assets/admin-user/"
        params = {"name": self.name}
        res = HTTP.get(url, params=params)
        res_data = res.json()
        if res.status_code in [200, 201] and res_data:
            self.id = res_data[0].get("id")
        return self.id


class Asset(object):
    # jumpserver 创建资产
    def __init__(self):
        self.id = None
        self.name = asset_name
        self.ip = asset_ip
        self.platform = asset_platform
        self.protocols = asset_protocols
        self.admin_user = AdminUser().get()
        self.node = Node().get()

    def exist(self):
        url = "/api/v1/assets/assets/"
        params = {"hostname": self.name}
        res = HTTP.get(url, params)
        res_data = res.json()
        if res.status_code in [200, 201] and res_data:
            self.id = res_data[0].get("id")
        else:
            self.create()

    def create(self):
        print("创建资产 {}".format(self.ip))
        url = "/api/v1/assets/assets/"
        data = {
            "hostname": self.name,
            "ip": self.ip,
            "platform": self.platform,
            "protocols": self.protocols,
            "admin_user": self.admin_user,
            "nodes": [self.node],
            "is_active": True,
        }
        res = HTTP.post(url, json=data)
        self.id = res.json().get("id")

    def perform(self):
        self.exist()


class APICreateAsset(object):
    def __init__(self):
        self.jms_url = jms_url
        self.username = jms_username
        self.password = jms_password
        self.token = None
        self.server = None

    def init_http(self):
        HTTP.server = self.jms_url
        HTTP.get_token(self.username, self.password)

    def perform(self):
        self.init_http()
        self.perm = Asset()
        self.perm.perform()


if __name__ == "__main__":
    # jumpserver url 地址
    jms_url = "http://127.0.0.1:8000"

    host_list = sys.argv[1].split(",")
    inventory_location = sys.argv[2]
    host_type = sys.argv[3]
    # 资产管理用户
    assets_admin_name = sys.argv[4]
    # 资产节点
    asset_node_name = sys.argv[5]
    # 管理员账户
    jms_username = "admin"
    jms_password = "j5helSdt91DKDxv5"

    for host in host_list:
        # 资产信息
        asset_name = str(inventory_location) + "-" + str(host).replace(".", "-") + "-" + str(host_type)
        asset_ip = host
        asset_platform = "Linux"
        asset_protocols = ["ssh/28000,sftp/28000"]

        api = APICreateAsset()
        api.perform()
